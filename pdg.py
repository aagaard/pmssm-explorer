from dash.html import Span,Sub,Sup

partNames={
    1 : "d",
    2 : "u",
    3 : "s",
    4 : "c",
    5 : "b",
    6 : "t",
    11 : "e",
    12 : ["ν",Sub("e")],
    13 : "μ",
    14 : ["ν",Sub("μ")],
    15 : "τ",
    16 : ["ν",Sub("τ")],
    21 : "g",
    22 : "γ",
    23 : "Z",
    24 : "W",
    25 : "h",
    35 : ["H",Sup("0")],
    36 : ["A",Sup("0")],
    37 : ["H",Sup("+")],
    111 : ["π",Sup("0")],
    211 : ["π",Sup("+")],
    1000021    : Span("g",className="susy"),
    1000011    : [Span("e",className="susy"),Sub("L")],
    1000012    : [Span("ν",className="susy"),Sub("e")],
    1000013    : [Span("μ",className="susy"),Sub("L")],
    1000014    : [Span("ν",className="susy"),Sub("μ")],
    1000015    : [Span("τ",className="susy"),Sub("L")],
    1000016    : [Span("ν",className="susy"),Sub("τ")],
    2000011    : [Span("e",className="susy"),Sub("R")],
    2000013    : [Span("μ",className="susy"),Sub("R")],
    2000015    : [Span("τ",className="susy"),Sub("R")],
    1000022    : [Span("χ",className="susy"),Sub("1"),Sup("0")],
    1000023    : [Span("χ",className="susy"),Sub("2"),Sup("0")],
    1000024    : [Span("χ",className="susy"),Sub("1"),Sup("+")],
    1000025    : [Span("χ",className="susy"),Sub("3"),Sup("0")],
    1000035    : [Span("χ",className="susy"),Sub("4"),Sup("0")],
    1000037    : [Span("χ",className="susy"),Sub("2"),Sup("+")],
    1000001    : [Span("d",className="susy"),Sub("L")],
    1000002    : [Span("u",className="susy"),Sub("L")],
    1000003    : [Span("s",className="susy"),Sub("L")],
    1000004    : [Span("c",className="susy"),Sub("L")],
    1000005    : [Span("b",className="susy"),Sub("1")],
    1000006    : [Span("t",className="susy"),Sub("1")],
    2000001    : [Span("d",className="susy"),Sub("R")],
    2000002    : [Span("u",className="susy"),Sub("R")],
    2000003    : [Span("s",className="susy"),Sub("R")],
    2000004    : [Span("c",className="susy"),Sub("R")],
    2000005    : [Span("b",className="susy"),Sub("2")],
    2000006    : [Span("t",className="susy"),Sub("2")],
    }

pdgMasses={ 1: 0.33,
         2: 0.33,
         3: 0.5,
         4: 1.5,
         5: 4.8,
         6: 160,  #we allow top to be somewhat offshell
         11: 0.00051,
         12: 0,
         13: 0.10566,
         14: 0,
         15: 1.777,
         16: 0,
         21: 0,
         22: 0,
         23: 91.2, #we allow Z to be offshell
         24: 80.4, #we allow W to be offshell
         111: 0.13498,
         211: 0.13957,
         }

def htmlName(pid):
    if abs(pid) in partNames:
        name=partNames[abs(pid)]
        if not (type(name) is list):
            name=[name]
        return name
    print("ERROR - unknown pdg id:",pid)
    return str(pid)

def partMass(pid,massBlock):
    if abs(pid) in massBlock.entries:
        return abs(massBlock.entries[abs(pid)])
    if abs(pid) in pdgMasses:
        return abs(pdgMasses[abs(pid)])
    print("Unknown id:",pid)
