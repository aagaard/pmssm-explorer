#!/usr/bin/env python

from collections import defaultdict
import os
import sys

import db
from flask import Flask
import pandas
import pyslha

CHUNK=1000

def getMassData(dbname):
    modelData=db.getModels(dbname)
    models,modelNames=zip(*modelData)
    numModels=len(models)
    if models[-1]!=numModels:
        raise Exception("last model number is not the same as the number of models!")
    
    masses=defaultdict(list)
    for idx in range(1,numModels+1,CHUNK):
        print(f"Reading masses at {idx}-{idx+CHUNK}")
        slhas=db.getTextResults(dbname,'slha',idx,idx+CHUNK)
        for model,text in slhas:
            if text:
                pyslhaDoc=pyslha.readSLHA(text,ignorenomass=True,ignoreblocks=["SPINFO"])
                mm=pyslhaDoc.blocks['MASS'].entries
                masses['N1'].append(abs(mm[1000022]))
                masses['N2'].append(abs(mm[1000023]))
                masses['C1'].append(mm[1000024])
                masses['DM'].append(mm[1000024]-abs(mm[1000022]))
                masses['LSQ'].append(min([mm[1000001],mm[2000001],mm[2000002]])) # assumes PMSSM-19
                masses['LSL'].append(min(mm[1000011],mm[2000011]))              # assumes PMSSM-19
                masses['TAU'].append(mm[1000015])
                masses['T1'].append(mm[1000006])
                masses['B1'].append(mm[1000005])
                masses['3D'].append(min(mm[1000005],mm[1000006]))
                masses['GL'].append(mm[1000021])
            else:
                masses['N1'].append(2000)
                masses['N2'].append(0)
                masses['C1'].append(0)
                masses['DM'].append(0)
                masses['LSQ'].append(0)
                masses['LSL'].append(0)
                masses['TAU'].append(0)
                masses['T1'].append(0)
                masses['B1'].append(0)
                masses['3D'].append(0)
                masses['GL'].append(0)
                print("No masses for:",model)
                

    masses['modelName']=modelNames
    data=pandas.DataFrame(masses)
    return data

def getCLsData(dbname):
    modelData=db.getModels(dbname)
    models,modelNames=zip(*modelData)
    numModels=len(models)
    steps=db.getSteps(dbname)
#    fitSteps=[step for step in steps if step.endswith("Fitter") or step.endswith("CLs")]
    fitSteps=[step for step in steps if step.endswith("Summary")]
    results={}
    dummy,llvstatus=zip(*db.getStatus(dbname,'LLVFilter'))
    llvstatus=[0 if not stat else stat for stat in llvstatus]
    for step in fitSteps:
        dummy,status=zip(*db.getStatus(dbname,step))
        status=[0 if not stat else stat for stat in status]
        cols,rows=db.getTableStruct(dbname,step)[0]
        cols=cols.split(",")
        rows=rows.split(",")
        numCols=len(cols)
        goodRows=[]
        idxHiggsMass=rows.index("Higgs_mass_constraint")
        idxLEP=rows.index("LEP_constraint")
        idxFlavour=rows.index("Flavour_constraints")
        idxEWPrec=rows.index("EW_precision_constraints")
        idxDM=rows.index("DM_constraints")
        idxStatus=rows.index("FourLepton2018")  #FIXME: this is currently best way to figure out if analysis was run

        for idx,row in enumerate(rows):
            #if row.endswith("2018") or row.endswith("2020"):
                goodRows.append( (idx,row) )            
        goodCols=[None,None,None]
        for idx,col in enumerate(cols):
            if col.endswith("ObsCLs"): goodCols[0]=idx
            if col.endswith("ExpCLs"): goodCols[1]=idx
            if col.endswith("Type"): goodCols[2]=idx
        if None in goodCols:
            print(f"Did not find CLs columns in {step} - will skip")
            continue

        analysisResults={}
        for idx,analysis in goodRows:
            analysisResults[analysis]={"ExpCLs": [],
                                       "ObsCLs": [],
                                       "Type": [],
                                       "LEP_Higgs": [],
                                       "Flavour": [],
                                       "EWPrec": [],
                                       "DMconstraint": [],
                                       "modelName": modelNames,
                                       "status": []}
            
        for idx in range(1,numModels+1,CHUNK):
            print(f"Reading {step} at {idx}-{idx+CHUNK}")

            data=db.getVectorResults(dbname,step,idx,idx+CHUNK)
            miss=False
            for ridx,analysis in goodRows:
                obsIdx=ridx*numCols+goodCols[0]
                expIdx=ridx*numCols+goodCols[1]
                typeIdx=ridx*numCols+goodCols[2]

                obsResults=analysisResults[analysis]["ObsCLs"]
                expResults=analysisResults[analysis]["ExpCLs"]
                typeResults=analysisResults[analysis]["Type"]
                LEPResults=analysisResults[analysis]["LEP_Higgs"]
                FlavResults=analysisResults[analysis]["Flavour"]
                EWPrecResults=analysisResults[analysis]["EWPrec"]
                DMResults=analysisResults[analysis]["DMconstraint"]
                statusResults=analysisResults[analysis]["status"]
                num=-1
                for dat in data:
                    num+=1
                    if idxLEP*numCols+goodCols[0]<len(dat):
                        LEPHiggs=min(float(dat[idxLEP*numCols+goodCols[0]]),float(dat[idxHiggsMass*numCols+goodCols[0]]))
                        Flavour=float(dat[idxFlavour*numCols+goodCols[0]])
                        EWPrec=float(dat[idxEWPrec*numCols+goodCols[0]])
                        DM=float(dat[idxDM*numCols+goodCols[0]])
                        status=float(dat[idxStatus*numCols+goodCols[0]])
                        if idx+num<len(llvstatus) and llvstatus[idx+num-1]<10:
                            status=3
                        LEPResults.append(LEPHiggs)
                        FlavResults.append(Flavour)
                        EWPrecResults.append(EWPrec)
                        DMResults.append(DM)
                        statusResults.append(status)
                    else:
                        LEPResults.append(1)
                        FlavResults.append(1)
                        EWPrecResults.append(1)
                        DMResults.append(1)
                        statusResults.append(2)
                        
                    if obsIdx<len(dat) and dat[obsIdx]:
                        obsResults.append(float(dat[obsIdx]))
                        expResults.append(float(dat[expIdx]))
                        typeResults.append(int(dat[typeIdx]))
                    else:
                        obsResults.append(1)
                        expResults.append(1)
                        typeResults.append(0)
        analysisPandas={}
        for analysis in analysisResults:
            analysisPandas[analysis]=pandas.DataFrame(analysisResults[analysis])
        results[step]=analysisPandas
    return results
    
if __name__ == "__main__":
    dbname=sys.argv[1]
    os.system(f"mkdir -p cache/{dbname}")
    app = Flask(__name__)
    with app.app_context():
        if len(sys.argv)>2:
            results=getMassData(dbname)
            results.to_feather(f"cache/{dbname}/models.feather")
        results=getCLsData(dbname)
        for step in results:
            for analysis in results[step]:
                oname=step.replace("Fitter","").replace("RecastCLs","Reco")
                os.system(f"mkdir -p cache/{dbname}/{oname}")
                results[step][analysis].to_feather(f"cache/{dbname}/{oname}/{analysis}.feather")
            
    
