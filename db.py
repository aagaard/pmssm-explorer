#!/usr/bin/env python

import os
import sys

import mysql.connector
from flask import g

import pyslha


STOP=-1
PENDING=0
RUNNING=1
DONE=10

def openDB(dbname):
    if 'db' not in g:
        g.db={}
    if not dbname in g.db:
        print("Opening:",dbname)
        g.db[dbname] = mysql.connector.connect(host=os.getenv("DBHOST"),
                                              user=os.getenv("DBUSER"),
                                              port=os.getenv("DBPORT"),
                                              passwd=os.getenv("DBPW"),
                                              database=dbname)
    return g.db[dbname]

def closeDBs():
    if 'db' not in g:
        return
    for name in g.db:
        print("Closing: ",name)
        g.db[name].close()
        


def getModels(dbname):
    mc=openDB(dbname).cursor()
    mc.execute("SELECT model,modelName FROM `models` ORDER by model")
    results=mc.fetchall()
    return results

def getTableStruct(dbname,step=None):
    mc=openDB(dbname).cursor()
    if not step:
        mc.execute("SELECT `step`,`columns`,`rows` FROM `tableStruct`")
    else:
        mc.execute("SELECT `columns`,`rows` FROM `tableStruct` WHERE `step`='"+step+"'")
    results=list(mc.fetchall())
    return results

def getColsRows(dbname,step):
    cols,rows=getTableStruct(dbname,step)[0]
    cols=cols.split(",")
    rows=rows.split(",")
    return cols,rows

def getSteps(dbname):
    mc=openDB(dbname).cursor()
    mc.execute("SELECT `step` FROM `tableStruct`")
    results=mc.fetchall()
    return [step[0] for step in results]

def getStatus(dbname,step):
    mc=openDB(dbname).cursor()
    mc.execute("SELECT model,"+step+" FROM `models` ORDER BY model")
    results=mc.fetchall()
    return results

def getModelStatus(dbname,model):
    mc=openDB(dbname).cursor(dictionary=True)
    mc.execute(f"SELECT * FROM `models` WHERE model = {model}")
    results=mc.fetchall()
    return results

def getDone(dbname,step):
    mc=openDB(dbname).cursor()
    mc.execute("SELECT model FROM `models` where "+step+f">{DONE} ORDER BY model")
    results=mc.fetchall()
    return [res[0] for res in results]

def getTextResults(dbname,step,first,last=None):
    mc=openDB(dbname).cursor()
    if last:
        mc.execute("SELECT model,vals FROM `"+step+f"` WHERE model>={first} AND model < {last} ORDER BY model")
    else:
        mc.execute("SELECT model,vals FROM `"+step+f"` WHERE model={first}")
    results=mc.fetchall()
    return results

def getTextResult(dbname,step,first):
    return getTextResults(dbname,step,first)[0][1]

def getVectorResults(dbname,step,first,last=None):
    results=getTextResults(dbname,step,first,last)
    data=[]
    for result in results:
        data.append(result[1].split(","))
    return data


if __name__ == "__main__":
    dbname=sys.argv[1]
    models=getModels(dbname)
    print(f"Found: {len(models)} models")
    steps=getSteps(dbname)
    print(f"Found: {len(steps)} steps")
    if len(sys.argv)>2:
        step=sys.argv[2]
        status=getStatus(dbname,step)
        done=getDone(dbname,step)
        print(f"Found: {len(done)} done models")
#        data=getTextResults(dbname,step,1)
#        print(data)
    slha=getTextResults(dbname,'slha',1)
    inputText=slha[0][1]
    pyslhaDoc=pyslha.readSLHA(inputText,ignorenomass=True)
    print(pyslhaDoc.blocks['MASS'])
    
        
