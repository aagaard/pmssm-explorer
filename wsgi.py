#!/usr/bin/env python

from collections import defaultdict
from dash import Dash, html, dcc, callback, Output, Input, MATCH, ALL, dash_table, ctx, clientside_callback
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc
#import plotly.express as px
import os
import plotly.graph_objects as go # or plotly.express as px
import pandas as pd
import json
import pyslha

import db
import pdg

def nonzero(cls):
    return max(cls,1e-10)

print("Reloading")

if not os.access("cache",os.R_OK):
    print("Setting up cache - please stand by")
    os.system("ln -sfT /data cache")
    if not os.access("cache/BinopMSSMRun2",os.R_OK):
        os.system("python prepareDB.py BinopMSSMRun2")

plottypes={ "Gluino": ("N1","GL",1800,3000),
            "Squark": ("N1","LSQ",1800,3000),
            "Stop": ("N1","T1",1200,2000),
            "Sbottom": ("N1","B1",1200,2000),
            "3rd": ("N1","3D",1200,3000),
            "Slepton": ("N1","LSL",1000,1500),
            "Stau": ("N1","TAU",800,1000),
            "Chargino": ("N1","C1",800,1200),
            "Neutralino": ("N1","N2",800,1200),
            "Compressed": ("DM","C1",100,1000),
}

orderedAnalysis={'Overall': 'ATLAS Exclusion',
                 'Search': 'SUSY Searches',
                 'OverallSmear': 'Truth-level searches',
                 'DisappearingTrack': 'Disappearing track',
                 'EwkCompressed2018_withBins': "EW compressed",
                 'EwkFullHad2018': "EW all-hadronic",
                 'EwkOneLeptonTwoBjets2018': "EW 1-lepton, 2-bjets",
                 'EwkTwoLeptonZeroJet2018': "EW 2-leptons, 0-jets",
                 'EwkTwoLeptonTwoJet2018': "EW 2-leptons, 2-jets", 
                 'EwkThreeLeptonOffshell2018': "EW 3-lepton, off-shell",
                 'EwkThreeLeptonOnshell2018': "EW 3-lepton, on-shell",
                 'FourLepton2018': "4-lepton",
                 "ZeroLeptonBDT2018":"0-lepton BDT",
                 "ZeroLeptonDiscovery2018":"0-lepton discovery",
                 "ZeroLeptonMultibin2018":"0-lepton multibin",
                 'MultiJets2018': "0-lepton 7+ jets",
                 'OneLepton2018': "1-lepton inclusive",
                 "StrongTwoLeptonTwoJet2018":"Strong 2-lepton, 2-jets",
                 'SameSignThreeLepton2018': "Same-sign 2L, 3L, 1st wave",
                 "SS3Lstrong2020": "Same-sign 2L, 3L, 2nd wave",
                 'MultiBjets2018': "Multi-bjets",
                 "SbottomMultiB2018": "Sbottom multi-B",
                 "SbottomTau2018": "Sbottom to tau",
                 "ttbarMET0L2018": "Stop 0-lepton",
                 "StopOneLepton2018": "Stop 1-lepton",
                 "StopTwoLepton2018": "Stop 2-lepton",
                 "StopZ2018": "Stop with Z-boson",
                 "Stoph2018": "Stop with h-boson",
                 "StopStau2018": "Stop to stau",
                 'h_inv': "Higgs to invisible BF",
                 'm_A': "Pseudo scalar Higgs (couplings)",
                 'ggA_constraint': "ggA, A->tautau search",
                 'bbA_constraint': "bbA, A->tautau search",
                 'All_external_constraints': "All external constraints",
                 'nonDM_constraints': "Non-DM external constraints",
                 'DM_constraints': "DM constraints",
                 'EW_precision_constraints': "EW precision measurements",
                 'Flavour_constraints': "Flavour constraints",
                 'Higgs_mass_constraint': "Higgs mass constraint",
                 'LEP_constraint': "LEP chargino constraint",
                 }

scanLabels={'Run2General': 'General scan',
            'Run2GenBino': 'General Bino scan',
            'Run2Stop':    'Stop scan',
            'Run2StopBino':'Stop Bino scan',
            'Run1Models':  'Run 1 model scan ',
            'EWpMSSMRun2': 'EW scan',
            'BinopMSSMRun2': 'EW bino scan'
            }

axisLabels={"N1": r"$\textrm{Mass of }\chi_{1}^{0} \textrm{ [GeV]}$",
            "N2": r"$\textrm{Mass of }\chi_{2}^{0} \textrm{ [GeV]}$",
            "C1": r"$\textrm{Mass of }\chi_{1}^{+} \textrm{ [GeV]}$",
            "DM": r"$M\chi^{+}_{1})-M(\chi_{0}^{1}) \textrm{ [GeV]}$",
            "LSL": "Mass of lightest first generation slepton [GeV]",
            "TAU": "Mass of stau [GeV]",
            "LSQ": "Mass of lightest first generation squark [GeV]",
            "GL":  "Mass of gluino [GeV]",
            "T1":  "Mass of lightest stop [GeV]",
            "B1":  "Mass of lightest sbottom [GeV]",
            "3D":  "Mass of lightest stop/sbottom [GeV]"}

dbs={}
# get available databases
for dbname in os.listdir("cache"):
    dbs[dbname]={}
    for step in os.listdir(f"cache/{dbname}"):
        if step.endswith(".feather"): continue
        dbs[dbname][step]=[name.replace(".feather","") for name in os.listdir(f"cache/{dbname}/{step}")]
print(dbs)

app = Dash(__name__,suppress_callback_exceptions=True)#,external_stylesheets=[dbc.themes.BOOTSTRAP])
app.title="ATLAS PMSSM Explorer"
application=app.server #for gunicorn

styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}

dbnames=[]
for scan in scanLabels:
    if scan in dbs:
        dbnames.append({'label':scanLabels[scan],'value':scan})
for scan in dbs:
    if not scan in scanLabels:
        print("No name for",scan)

#dbnames=sorted(dbs.keys(),reverse=True)
prodnames=sorted(dbs[dbnames[0]['value']].keys()) # we display first scan by default
if prodnames:
    analysisnames=sorted(dbs[dbnames[0]['value']][prodnames[0]]) # we display first scan by default
    firstanalysis=analysisnames[0]
else:
    analysisnames=[]
    firstanalysis=""
    prodnames=["TBA"]
print("allo:",analysisnames)
if "Overall" in analysisnames:
    firstanalysis="Overall"
app.layout = html.Div(["Contact: Brian.Petersen@cern.ch",
    html.H1(children='pMSSM Explorer', style={'textAlign':'center'}),
    html.Div([
        html.Div([
            "Scan:",
            dcc.Dropdown(dbnames,dbnames[0]['value'],clearable=False,id='scan-selector')],
                 style={'width': '30%'}),
        html.Div([
            'Production type:',
            dcc.Dropdown(prodnames, prodnames[0], clearable=False, id='production-selector')],
                 style={'width': '30%'}),
    ],style={'display': 'flex'}),
    html.Div([
        html.Div([
            'Plot type:',
            dcc.Dropdown(list(plottypes.keys()), 'Gluino', clearable=False,id='plot-selector'),
            ],style={'width': '15%'}),
        html.Div([
            'Binning:',
            dcc.Dropdown([1,2,4], 1, clearable=False,id='bin-selector'),
            ],style={'width': '15%'}),
        html.Div([
            'Analysis results:',
            dcc.Dropdown(analysisnames, firstanalysis, clearable=False, id='analysis-selector'),
            ],style={'width': '30%'}),
        ],style={'display': 'flex'}
        ),
    dcc.RadioItems(['Excluded Fraction','Max CLs','Min CLs','Model count'],'Max CLs',id='plot-var',inline=True),
    dcc.RadioItems(['Exp. CLs','Obs. CLs'],'Exp. CLs',id='cls-type',inline=True),
    dcc.Checklist(options=[{'label': 'Only good models', 'value': 'onlyproc'},
                           {'label': 'LEP & m(H)', 'value': 'HiggsLep'},
                           {'label': 'EW prec.', 'value': 'EWPrec'},
                           {'label': 'Flavour', 'value': 'Flavour'},
                           {'label': 'DM', 'value': 'DM'},
                           ],
                  value=['onlyproc','HiggsLep'],id='proc-only',inline=True),
    html.Div([
        html.Div([dcc.Graph(id='graph-content',mathjax=True,style={'width': '100%','display': 'inline-block'})]),
        html.Div([dash_table.DataTable(
            id='modeltable',
            sort_action="native",
            style_header={
                'textAlign': 'center'
            },
            style_as_list_view=True,
            page_size=10,
            row_selectable='single',
            style_cell={
                # all three widths are needed
                'minWidth': '180px', 'width': '180px', 'maxWidth': '180px'
                },
            style_data_conditional=[{
                'if': {
                    'filter_query': '{status} > 1',
                },
                'backgroundColor': 'tomato',
                'color': 'white'
            }],
            columns=[{"name": "model", "id": "modelName"},
                     #{"name": "status","id": "status"},
                     {"name": "Obs. CLs", "id": "ObsCLs"},
                     {"name": "Exp. CLs", "id": "ExpCLs"},
            ]),
        ],style={'width': '30%', 'display': 'inline-block'}),
        ],style={'display': 'flex'}),
    html.H3(f"Model: ",id="model-id"),
    html.Div([
        html.Div(id='modelinfo',style={'width': '35%', 'display': 'inline-block'}),
        html.Div(html.Div([dcc.Dropdown(['slha','PreFilter'],'slha',clearable=False,id='detail-selector'),
                           html.Div(id='detailed-display')]),
                 style={'width': '50%', 'display': 'inline-block'})
        ],style={'display': 'flex'})
    ])

@app.server.teardown_appcontext
def teardown_db(exception):
    db.closeDBs()

@callback(
    Output('production-selector','options'),
    Output('modeltable', 'data', allow_duplicate=True),
    Output('modeltable', 'selected_rows',allow_duplicate=True),
    Output('graph-content', 'clickData'),
#    Output('detail-selector','options'),
    Input('scan-selector','value'),
    prevent_initial_call=True
)
def selectScan(dbname):
    print("selectScan",dbname)
    prodnames=sorted(dbs[dbname].keys())
 #   steps=db.getSteps(dbname)
    return prodnames,[],[],None#,steps

@callback(
    Output('production-selector','value'),
    Input('production-selector','options')
)
def setProdDefault(options):
    print("setDefaultProd",options)
    return options[0]


@callback(
    Output('plot-selector','value'),
    Input('plot-selector','options'),
    Input('production-selector','value')
)
def setProdDefault(options,prodname):
    print("setDefaultPlot",options)
    if "QCD" in prodname: return "Gluino"
    if "EW" in prodname: return "Chargino"
    if "Stop" in prodname: return "Stop"
    return options[0]



@callback(
    Output('analysis-selector','options'),
    Input('scan-selector','value'),
    Input('production-selector','value')
)
def selectProduction(dbname,prodname):
    print("selectProd",dbname,prodname)
    analysisnames=sorted(dbs[dbname][prodname])
    options=[]
    for name in orderedAnalysis:
        if name in analysisnames:
            options.append({"label": orderedAnalysis[name],
                            "value": name})
            del analysisnames[analysisnames.index(name)]
    print("Not listed analysis:",analysisnames)
    return options

@callback(
    Output('analysis-selector','value'),
    Input('analysis-selector','options')
)
def setAnaDefault(options):
    print("setDefaultAnalysis",options)
    return "Overall"
#    if "Search" in options: return "Search"
#    return options[0]


@callback(
    Output('graph-content', 'figure'),
    Input('plot-selector', 'value'),
    Input('plot-var', 'value'),
    Input('cls-type', 'value'),
    Input('scan-selector','value'),
    Input('production-selector','value'),
    Input('analysis-selector','value'),
    Input('proc-only','value'),
    Input('bin-selector','value')
)
def update_graph(plot_type,plot_var,cls_type,dbname,prodname,analysisname,proconly,binning):
    print("update_graph",plot_type,plot_var,cls_type,dbname,prodname,analysisname,proconly,binning)
    df=pd.concat([pd.read_feather(f"cache/{dbname}/models.feather"),
                  pd.read_feather(f"cache/{dbname}/{prodname}/{analysisname}.feather")],axis=1)
    df['id']=[ii for ii in range(len(df.status))]
    df.set_index('id', inplace=True, drop=False)
    if 'onlyproc' in proconly:
        df=df[df.status<=1]
    if 'HiggsLep' in proconly:
        df=df[df.LEP_Higgs>0]
    if 'EWPrec' in proconly:
        df=df[df.EWPrec>0]
    if 'Flavour' in proconly:
        df=df[df.Flavour>0]
    if 'DM' in proconly:
        df=df[df.DMconstraint>0]
    numModels=len(df)
    scale=[[0,'rgb(255,255,255)'],[1e-31,"rgb(0,0,0)"],[0.05,"rgb(0,0,255)"],[0.1,"rgb(0,255,0)"],[1,"rgb(255,0,0)"]]
    maxz=1
    if plot_var=='Model count':
        scale=[[0,'rgb(255,255,255)'],[1e-31,"rgb(0,255,0)"],[1,"rgb(255,0,0)"]]
        maxz=None
#        scale='Viridis'
    if plot_var=='Excluded Fraction':
        scale=[[0,'rgb(255,255,255)'],[1e-31,"rgb(100,0,0)"],[0.05,"rgb(255,0,0)"],[0.8,"rgb(0,255,0)"],[0.999,"rgb(0,0,255)"],[1,"rgb(0,0,0)"]]
    func={'Excluded Fraction':'avg',
          'Max CLs':'max',
          'Min CLs':'min',
          'Model count':'count'}[plot_var]
    xvar=plottypes[plot_type][1]
    yvar=plottypes[plot_type][0]
    xmax=plottypes[plot_type][3]
    ymax=plottypes[plot_type][2]
    if xvar=='3D': xexp=f'T1<{xmax} & B1' # hackaround stupid variable name
    else: xexp=xvar
    
    df=df.query(f'{xexp}<{xmax} & {yvar}<{ymax}')

    xvars=df[xvar]
    yvars=df[yvar]

    zvals=df[cls_type[:3]+'CLs'].map(nonzero)
    if func=='avg':
        zvals=zvals.transform(lambda cls: 1*(cls<0.05)+1e-10)
    
    fig = go.Figure(go.Histogram2d(
        x=xvars,
        y=yvars,
        z=zvals,
        histfunc=func,
        zmax=maxz,
        zmin=1e-10,
        xhoverformat=".0f",
        yhoverformat=".0f",
        zhoverformat=".3g",
        customdata=df.id,
#        hovertemplate=["gl: hello"],
#               "chi10: %{y:.0f}",
#               "CLs: %{z:.2g}"],
        colorscale=scale,
        nbinsx=20*binning, nbinsy=20*binning, 
        
    ))
    fig.update_layout(
        title=f"{dbname} - {prodname} - {analysisname} - {numModels} models",
        xaxis_title=axisLabels[plottypes[plot_type][1]],
        yaxis_title=axisLabels[plottypes[plot_type][0]],
    )
    return fig


@callback(
    Output('modeltable', 'selected_rows'),
    Output('modeltable', 'data'),
    Input('graph-content', 'clickData'),
    Input('scan-selector','value'),
    Input('production-selector','value'),
    Input('analysis-selector','value'),
    Input('proc-only','value'))
def display_click_data(clickData,dbname,prodname,analysisname,proconly):
    print("click_data",clickData,dbname,prodname,analysisname)
    if not clickData: return [],[]
    df=pd.read_feather(f"cache/{dbname}/{prodname}/{analysisname}.feather")
    df['id']=[ii for ii in range(len(df.status))]
    df.set_index('id', inplace=True, drop=False)
    points=clickData['points'][0]['customdata']
    df=df.loc[points]
    if 'onlyproc' in proconly:
        df=df[df.status<=1]
    if 'HiggsLep' in proconly:
        df=df[df.LEP_Higgs>0]
    if 'EWPrec' in proconly:
        df=df[df.EWPrec>0]
    if 'Flavour' in proconly:
        df=df[df.Flavour>0]
    if 'DM' in proconly:
        df=df[df.DMconstraint>0]
#    df.set_index('id',list(range(len(df))))
    return [],df.to_dict('records')

@callback(
    Output('modelinfo','children'),
    Input('modeltable','derived_virtual_selected_row_ids'),
    Input('scan-selector','value'),
    Input('production-selector','value'))
def model_select(rownum,dbname,prodname):
    print('select',rownum,dbname,prodname)
    if not rownum:
        raise PreventUpdate

#    if 'Reco' in prodname:
#        step=prodname.replace('Reco','RecastCLs')
#    else:
#        step=prodname+"Fitter"
    step=prodname
    cols,rows=db.getColsRows(dbname,step)
    numCols=len(cols)
    goodCols=[None,None]
    for idx,col in enumerate(cols):
        if col.endswith("ObsCLs"): goodCols[0]=idx
        if col.endswith("ExpCLs"): goodCols[1]=idx
    if None in goodCols:
        print(f"ERROR Did not find CLs columns in {step} - will skip")
        raise PreventUpdate
    data=db.getVectorResults(dbname,step,rownum[0]+1)

    analysisCLs=[]
    SRVals=defaultdict(list)
    for ridx,row in enumerate(rows):
        sr=row 
        if '2016' in sr: continue
        if 'total' in sr: continue
        if sr=='Summary': continue
        obsIdx=ridx*numCols+goodCols[0]
        expIdx=ridx*numCols+goodCols[1]
        dat=data[0]
        if obsIdx<len(dat) and dat[obsIdx]:
            obsCLs=float(dat[obsIdx])
            expCLs=float(dat[expIdx])
        else:
            continue
        if '__' in sr:
            words=sr.split("__")
            SRVals[words[0]].append( (expCLs,obsCLs,words[1]) )
        else:
            analysisCLs.append( (expCLs,obsCLs,sr) )
    analysisCLs.sort()
    #    return html.Div("hello")
    table_header = [
        html.Thead(html.Tr([html.Th("Analysis"), html.Th("Obs CLs"), html.Th("Exp CLs")]))
    ]
    rows=[]
    idx=1
    for expCLs,obsCLs,analysisName in analysisCLs:
        row=html.Tr([
            html.Td(analysisName,id={"type":"icon","index":idx}),
            html.Td("%.3f" % obsCLs),
            html.Td("%.3f" % expCLs),
        ])
        if analysisName.startswith("Summary"):
            rows=[row]+rows
        else:
            rows.append(row)
        subidx=1
        SRVals[analysisName].sort()
        for SRExpCLs,SRObsCLs,SRName in SRVals[analysisName]:
            row=html.Tr([
                html.Td(SRName,id={"type": "hiddensr","index":idx,"sub":subidx}),
                html.Td("%.3f" % SRObsCLs),
                html.Td("%.3f" % SRExpCLs),
            ],hidden=True,id={"type":"hiddenrow","index":idx,"sub":subidx},className="sr")
            rows.append(row)
            subidx+=1
        idx+=1
    table_body = [html.Tbody(rows)]
    table=dbc.Table(table_header+table_body, responsive=True,bordered=True)

    return [table]

@callback(
    Output({"type": "hiddenrow", "index": MATCH, 'sub': ALL}, 'hidden'),
    Input({'type': 'icon', 'index': MATCH}, 'n_clicks'),
    Input({"type": "hiddensr", "index": MATCH, 'sub': ALL}, 'id'), prevent_initial_call=True)
def update_select_input(n_clicks,rows):
    if not n_clicks: raise PreventUpdate
                       
    return [n_clicks%2==0]*len(rows)



def mkParticleTable(dbname,model):

    slhaText=db.getTextResult(dbname,'slha',model)
    pyslhaDoc=pyslha.readSLHA(str(slhaText),ignorenomass=True,ignoreblocks=["SPINFO"])
    BLOCKS=pyslhaDoc.blocks
    DECAYS=pyslhaDoc.decays

    masses=BLOCKS["MASS"].entries
    LSPMass=abs(masses[1000022])
    particles=[]
    decays=defaultdict(list)
    for entry in masses:
        mass=abs(masses[entry])
        if (entry<1000000 and entry!=36 and entry!=25) or mass>2500: continue
        particles.append( (mass,DECAYS[entry].totalwidth,entry) )
        for sDecay in DECAYS[entry].decays:
            br=sDecay.br
            if br<0.01: continue
            decayString=[]
            for ii in sDecay.ids:
                decayString+=pdg.htmlName(ii)
            dm=mass
            for ii in sDecay.ids:
                dm-=pdg.partMass(ii,BLOCKS['MASS'])
            decays[entry].append( (br,dm,decayString) )

    table_header=[html.Thead(
        html.Tr([html.Th("Particle"),
                 html.Th("Mass"),
                 html.Th("Width"),
                 html.Th("ΔM")]))]
    particles.sort()
    rows=[]
    idx=1
    for mass,width,pid in particles:
        partName=pdg.htmlName(pid)
        row=html.Tr([
            html.Td(partName,className="pmssm",id={"type":"part","index":idx}),
            html.Td(f'{mass:.1f} GeV',className="pmssm"),
            html.Td(f'{width:.2g} GeV',className="pmssm"),
            html.Td(f'{mass-LSPMass:.2f} GeV',className="pmssm")],id=f'tab-toggle-{pid}',className="pmssm")
        rows.append(row)
        decays[pid].sort(reverse=True,key=lambda dec: dec[0])
        subidx=1
        for br,dm,decayString in decays[pid]:
            row=html.Tr([
                html.Td(decayString,id={"type": "hiddendecay","index":idx,"sub":subidx}),
                html.Td(),
                html.Td(f'{br*100:.0f}%'),
                html.Td(f'{dm:.2f} GeV')],className=f"sr",hidden=True,id={"type":"hiddendecrow","index":idx,"sub":subidx})
            rows.append(row)
            subidx+=1
        idx+=1
    table_body = [html.Tbody(rows)]
    table=dbc.Table(table_header+table_body, responsive=True,bordered=True,className="pmssm")
    return table

@callback(
    Output({"type": "hiddendecrow", "index": MATCH, 'sub': ALL}, 'hidden'),
    Input({'type': 'part', 'index': MATCH}, 'n_clicks'),
    Input({"type": "hiddendecay", "index": MATCH, 'sub': ALL}, 'id'), prevent_initial_call=True)
def update_select_input(n_clicks,rows):
    if not n_clicks: raise PreventUpdate
    return [n_clicks%2==0]*len(rows)

@callback(
    Output('detail-selector','options'),
    Output('model-id','children'),
    Input('modeltable','derived_virtual_selected_row_ids'),
    Input('scan-selector','value'),
    prevent_initial_call=True
)
def showTableOptions(rownum,dbname):
    print("showTableOptions",rownum,dbname)
    if not rownum or ctx.triggered_id=='scan-selector':
        raise PreventUpdate
    model=rownum[0]+1
    status=db.getModelStatus(dbname,model)[0]
    options=[]
    modelName="Unknown"
    for step in sorted(status):
        if step=='modelName':
            modelName=status[step]
        if step.startswith('model'): continue
        option={'label': step, 'value': step}
        if not status[step] or status[step]<db.DONE:
            option['disabled']=True
        options.append(option)
    return options,f"Model: {modelName}"

@callback(
    Output('detailed-display','children'),
    Input('detail-selector','value'),
    Input('modeltable','derived_virtual_selected_row_ids'),
    Input('scan-selector','value'),
    prevent_initial_call=True
)
def update_detailed(detailname,rownum,dbname):
    print('updated_detailed',detailname,rownum,dbname)
    print('ctx',ctx.triggered_id)
    if not rownum or ctx.triggered_id=='scan-selector':
        raise PreventUpdate
    model=rownum[0]+1
    if detailname!='slha':

        cols,rows=db.getColsRows(dbname,detailname)        
        numCols=len(cols)
        if len(cols)==1 and cols[0]=='':
            return html.Pre(db.getTextResult(dbname,detailname,model))
        dataVec=db.getVectorResults(dbname,detailname,model)[0]
        columns=[{"name":"",'id':"label"}]+[{"name":col,'id':col} for col in cols]
        data=[]
        for ridx,row in enumerate(rows):
            drow={"label":row}
            for idx,col in enumerate(cols):
                idx=ridx*numCols+idx
                if idx<len(dataVec):
                    drow[col]=dataVec[idx]
            data.append(drow)
                    
#        print(cols,rows,data)
        table=dash_table.DataTable(
            data,
            columns=columns,
            id='detail-table',
            sort_action="native",
            filter_action="native",
            style_cell={'textAlign': 'left'},
            style_header={
                'textAlign': 'center'
            },
            page_size=30,
            )
        return table
    table=mkParticleTable(dbname,model)
    return table

if __name__ == '__main__':
    app.run_server(debug=True)
